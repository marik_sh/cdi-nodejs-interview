const moment = require('moment');

const passwordUtil = require('../utils/password');
const UserModel = require('../models/User');
module.exports = (phone, token) => {
    return new Promise((resolve, reject) => {
        console.log('getting user by phone:', phone);
        console.log('token is:', token);
        hash = passwordUtil.hashPassword(token);
        console.log('hash is', hash)
        UserModel.findOne({
            "phone" : phone
        }, (err, user) => {
            console.log(user)
            console.log(user.passwordReset.reset_password_expires)
            let time = moment(user.passwordReset.password_reset_expires).format();
            console.log(time, moment())
            console.log(moment(time).diff(moment()))
            // if (moment(user.passwordReset.password_reset_expires).diff(moment()) > 0) {
            //     resolve(false);
            //     return;
            // }

            resolve(user.passwordReset.reset_password_token == hash)
            return;
        })

    })
}