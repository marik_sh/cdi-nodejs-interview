const mongoose = require('mongoose');
const moment = require('moment');
const randToken = require('rand-token')
const User = require('../models/User');
const passwordUtil = require('../utils/password');

/**
 * creates a password reset token for th euser by phone number
 * @param {string} phone Requesting users phone number
 */
module.exports = (phone) => {
    return new Promise((resolve, reject) => {
    console.log('Updating user where phone', phone);

    let token = randToken.generate(4);
    let hashedToken = passwordUtil.hashPassword(token);
    
    User.find({"phone": phone}, (err, user) => {
        console.log(user[0]._id);
        User.update({"_id" :user[0]._id}, {
            $set: {
                passwordReset: {
                    reset_password_token: hashedToken,
                    reset_password_expires: moment().add(2, 'hours')
                }
            }
        }, (err, raw) => {
            if (err) console.log(err);
            resolve(token)
        })
    })

    })
}