// index.js
var express = require("express"), cors = require('cors');
var bodyParser = require("body-parser");

var app = express();

app.use(cors());
app.use(bodyParser.json());

require("./routes")(app);

app.listen(process.env.PORT || 8080, function() {
    console.log("My API is running... PORT 8080");
});

module.exports = app;