const mongoose = require('mongoose');
const resetPasswordSchema = mongoose.Schema({
    reset_password_expires: Date,
    reset_password_token: String
});

const User = mongoose.model('user', {
    name: String,
    password: String,
    passwordReset: resetPasswordSchema
    }, 'users');

module.exports = User;