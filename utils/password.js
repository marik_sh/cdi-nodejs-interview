const crypto = require('crypto');

/**
 * Hash password with salt
 * @param {string} password password string
 * @param {string} salt salt hash
 */
const hashPassword = (password) => {
    password = crypto.createHash('md5').update(password).digest("hex");
    return password
};



/**
 * generates random string of characters i.e salt
 * @function
 * @param {number} length - Length of the random string.
 */
const generateSalt = (length) => {
    return crypto.randomBytes(Math.ceil(length / 2))
        .toString('hex') /** convert to hexadecimal format */
        .slice(0, length); /** return required number of characters */
};


/**
 * Salts and Hashes password
 * @param {string} password password to salt and hash
 */
const saltAndHash = (password) => {
    let salt = generateSalt(16); /** Gives us salt of length 16 */
    return hashPassword(password, salt);
};

module.exports = {
    saltAndHash,
    hashPassword
};
