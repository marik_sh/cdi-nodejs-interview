var mongoose = require('mongoose');
mongoose.connect('mongodb://cdi-interview:8Bz1eQpt@ds261521.mlab.com:61521/cdi-nodejs-interview');
let User = require('./models/User');
const resetPassword = require('./controllers/resetPassword');
const validateToken = require('./controllers/validateToken')

// expose the routes to our app with module.exports
module.exports = async function (app) {

  app
  .get("/", function (req, res) {

    res.json({
      status: "My API is alive!",
      models
    });
  })
  .get('/users/', (req, res) => {
    let findUsers = User.find(null, null, (err, users) => {
        res.json({
        status: 'OK',
        users
      })
    })
  })
  .post('/users/password', async (req, res) => {
    console.log(req.body.phone);
    let doc = await resetPassword(req.body.phone);

    res.json({
      successMessage: "True!",
      token: doc
    })
  })
  .post('/users/password/reset/', async (req,res) => {

    let answer = await validateToken(req.body.phone, req.body.token)
    console.log(answer);
    res.json({
      status: "ok",
      is_validated: answer
    })
  });

// find user by query, create token and set TTL of 2H
}
